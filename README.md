# Nayad Modular Datalogger

## The Nayad for Large-Scale Deployment

[Nayad Modular](https://hackingecology.gitlab.io/overview/hardware/) is a datalogger focused on low cost systems for water monitoring with temperature, conductivity and total dissolved solid (TDS).

The first prototype (the base of the current design) have been tested during 2019, although the conductivity circuits need improvements to a more robust and stable version. Thus, it is necessary the design of a module using the same schematic used in Nayad.

This is a perfect version to be adopted and deployed in Citizen/Community Science projects due its hyper-low costs. This is the version which we can develop different modules (pH, ORP, Dissolved Oxygen) to enable modularity to this datalogger version.

# Definitions: Sonde, Probe, Sensor and Microcontroller

## Sensor Module Input

The input allows users easily integrate a module of specific sensor: pH, Dissolved Oxygen, Conductivity, Total Dissolved Solid, etc.


![](https://gitlab.com/hacking-ecology/nayad-modular/-/raw/master/IMG/nayad_mod.png)

# Nayad Modular Specifications:

## The Modular PCB
- 16 analog pins
- 30 Digital pins
- Extra sensor module input (pH, ORP, TDS, Conductivity, turbidity or dissolved oxygen).

## Operation Current

- ESP32 - 45 mA
- DS18B20 (temperature) - 2 mA
- microSD - 200 µA (pending review)
- RTC: PCF2129AT - 0.65 µA


**Autonomy using a 3.7v (4000mah) Battery:** ~ 85:14 hours

----------------------


This documentation describes Nayad Modular v0.1 and is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). You may redistribute and modify this documentation under the terms of the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). 

Nayad Modular v0.1 is licensed using a [CERN-OHL-S V.2](http://ohwr.org/cernohl). It is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-S V.2 for applicable conditions.


![](https://gitlab.com/hacking-ecology/nayad-modular/-/raw/master/IMG/oshwa-nayad-modular.png)

Nayad Modular is OSHW certified Open Source Hardware with UID ES000018 

A copy of this license is provided in the LICENSE file.
